### API et représentation des données

Nous pouvons tout d'abord réfléchir à l'API REST que nous allons offrir pour la ressource *pizzas*. Celle-ci devrait répondre aux URI suivantes :

| URI                      | Opération   | MIME                                                         | Requête         | Réponse                                                              |
| :----------------------- | :---------- | :---------------------------------------------               | :--             | :----------------------------------------------------                |
| /pizzas             | GET         | <-application/json<br><-application/xml                      |                 | liste des pizzas (I2)                                           |
| /pizzas/{id}        | GET         | <-application/json<br><-application/xml                      |                 | un pizzas (I2) ou 404                                            |
| /pizzas/{id}/name   | GET         | <-text/plain                                                 |                 | le nom de pizza ou 404                                        |
| /pizzas             | POST        | <-/->application/json<br>->application/x-www-form-urlencoded | Pizzas (I1) | Nouvel Pizzas (I2)<br>409 si pizzas existe déjà (même nom) |
| /pizzas/{id}        | DELETE      |                                                              |                 |                                                                      |

Un pizzas comporte un identifiant ,un nom et une liste d'ingredients. Sa
représentation JSON (I2) prendra donc la forme suivante :

    {
      "id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "name": "Licorne"
      "ingredient": ["arcenciel", "corne"]
    }

on aura une représentation JSON (I1) qui comporte uniquement le nom et la liste d'ingredients:

    { 
    	"name": "Licorne" 
    	"ingredient": ["arcenciel", "corne"]
    }

