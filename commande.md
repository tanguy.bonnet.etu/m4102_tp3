### API et représentation des données

| URI            	   | Opération   | MIME 										 | Requête 		     | Réponse                                                            |
| :--------------	   | :---------- | :---------------------------------------------| :--               | :----------------------------------------------------------------- | 
| /commande      	   | GET         | <-application/json<br><-application/xml       |                   | liste des commandes (C2) et sa liste de pizzas                     |
| /commande/{id} 	   | GET         | <-application/json<br><-application/xml       |                   | une commande ou 404                                                |
| /commande/{id}/nom   | GET         | <-text/plain                                  |                   | le nom de l'acheteur ou 404                                        | 
| /commande            | POST        | <-/->application/json                         | Commande(C2)      | Nouvelle commande(C2)<br>409 si la commande existe déjà (même nom) |
| /commande/{id}       | DELETE      |                                               |                   |                                                                    |

Une commande comporte une liste des pizzas, un identifiant, un nom et un prénom et la liste de pizzas. Sa
représentation JSON (C2) prendra donc la forme suivante :

    {"id": "f38806a8-7c85-49ef-980c-149dcd81d306",
      "prénom":"Tanguy",
      "nom": "Bonnet",
      "list": ["Licorne","Chorizo"]}  
    }  

on aura une représentation JSON (C1) qui comporte uniquement le nom, prénom et la liste des pizzas :

    { "prénom":"Tanguy",
      "nom": "Bonnet",
      "list": ["Licorne","Chorizo"]}  
    }