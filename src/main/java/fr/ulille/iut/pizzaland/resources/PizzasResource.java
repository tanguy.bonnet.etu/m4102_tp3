package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import fr.ulille.iut.pizzaland.dto.PizzasCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzasDto;
import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Pizzas;
import fr.ulille.iut.pizzaland.dao.PizzasDao;

@Path("/pizzas")
public class PizzasResource {

	private static final Logger LOGGER = Logger.getLogger(PizzasResource.class.getName());
	//private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	private PizzasDao pizzas;

	@Context
	public UriInfo uriInfo;

	public PizzasResource() {
		pizzas = BDDFactory.buildDao(PizzasDao.class);
		pizzas.createPizzasTable();
	}

	@GET
	public List<PizzasDto> getAll() {
		LOGGER.info("PizzasResource:getAll");

		List<PizzasDto> l = pizzas.getAll().stream().map(Pizzas::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}

	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public PizzasDto getOnePizzas(@PathParam("id") UUID id) {
		LOGGER.info("getOnePizzas(" + id + ")");
		try {
			Pizzas pizza = pizzas.findById(id);
			LOGGER.info(pizza.toString());
			return Pizzas.toDto(pizza);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@POST
	public Response createPizzas(PizzasCreateDto pizzasCreateDto) {
		Pizzas existing = pizzas.findByName(pizzasCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizzas pizza = Pizzas.fromPizzasCreateDto(pizzasCreateDto);
			pizzas.insertPizzas(pizza);
			PizzasDto pizzasDto = Pizzas.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path(pizza.getId().toString()).build();

			return Response.created(uri).entity(pizzasDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}

	@DELETE
	@Path("{id}")
	public Response deletePizzas(@PathParam("id") UUID id) {
		if ( pizzas.findById(id) == null ) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		pizzas.removePizzas(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	@GET
	@Path("{id}/name")
	public String getPizzasName(@PathParam("id") UUID id) {
		Pizzas pizza = pizzas.findById(id);

		if (pizza == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return pizza.getName();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createPizzas(@FormParam("name") String name) {
		Pizzas existing = pizzas.findByName(name);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Pizzas pizza = new Pizzas();
			pizza.setName(name);

			pizzas.insertPizzas(pizza);

			PizzasDto pizzasDto = Pizzas.toDto(pizza);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + pizza.getId()).build();

			return Response.created(uri).entity(pizzasDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}

}