package fr.ulille.iut.pizzaland.resources;

import java.net.URI;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.FormParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.WebApplicationException;
import fr.ulille.iut.pizzaland.dto.CommandeCreateDto;
import fr.ulille.iut.pizzaland.dto.CommandeDto;
import fr.ulille.iut.pizzaland.BDDFactory;
import fr.ulille.iut.pizzaland.beans.Commande;
import fr.ulille.iut.pizzaland.dao.CommandeDao;

@Path("/commande")
public class CommandeResource {

	private static final Logger LOGGER = Logger.getLogger(CommandeResource.class.getName());
	//private static final Logger LOGGER = Logger.getLogger(IngredientResource.class.getName());
	private CommandeDao commande;

	@Context
	public UriInfo uriInfo;

	public CommandeResource() {
		commande = BDDFactory.buildDao(CommandeDao.class);
		commande.createCommandeTable();
	}

	@GET
	public List<CommandeDto> getAll() {
		LOGGER.info("CommandeResource:getAll");

		List<CommandeDto> l = commande.getAll().stream().map(Commande::toDto).collect(Collectors.toList());
		LOGGER.info(l.toString());
		return l;
	}
	@GET
	@Path("{id}")
	@Produces({ "application/json", "application/xml" })
	public CommandeDto getOneCommande(@PathParam("id") UUID id) {
		LOGGER.info("getOneCommande(" + id + ")");
		try {
			Commande commandes = commande.findById(id);
			LOGGER.info(commandes.toString());
			return Commande.toDto(commandes);
		} catch (Exception e) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}
	}

	@POST
	public Response createCommande(CommandeCreateDto commandeCreateDto) {
		Commande existing = commande.findByName(CommandeCreateDto.getName());
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Commande commandes = Commande.fromCommandeCreateDto(commandeCreateDto);
			commande.insertCommande(commandes);
			CommandeDto commandeDto = Commande.toDto(commandes);

			URI uri = uriInfo.getAbsolutePathBuilder().path(commandes.getId().toString()).build();

			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}

	}

	@DELETE
	@Path("{id}")
	public Response deleteCommande(@PathParam("id") UUID id) {
		if (commande.findById(id) == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		commande.removeCommande(id);

		return Response.status(Response.Status.ACCEPTED).build();
	}

	@GET
	@Path("{id}/name")
	public String getCommandeName(@PathParam("id") UUID id) {
		Commande commandes = commande.findById(id);

		if (commandes == null) {
			throw new WebApplicationException(Response.Status.NOT_FOUND);
		}

		return commandes.getName();
	}

	@POST
	@Consumes("application/x-www-form-urlencoded")
	public Response createCommande(@FormParam("nom") String nom) {
		Commande existing = commande.findByName(nom);
		if (existing != null) {
			throw new WebApplicationException(Response.Status.CONFLICT);
		}

		try {
			Commande commande = new Commande();
			commande.setName(nom);

			CommandeDto commandeDto = Commande.toDto(commande);

			URI uri = uriInfo.getAbsolutePathBuilder().path("" + commande.getId()).build();

			return Response.created(uri).entity(commandeDto).build();
		} catch (Exception e) {
			e.printStackTrace();
			throw new WebApplicationException(Response.Status.NOT_ACCEPTABLE);
		}
	}


}