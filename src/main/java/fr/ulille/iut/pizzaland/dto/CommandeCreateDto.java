package fr.ulille.iut.pizzaland.dto;

import java.util.List;

import fr.ulille.iut.pizzaland.beans.Pizzas;

public class CommandeCreateDto {
	private String prenom;
	private static String name;
	private List<Pizzas> pizzas;
		
	public CommandeCreateDto() {
		
	}
		
	public void setName(String name) {
		this.name = name;
	}
 		
	public static String getName() {
		return name;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
