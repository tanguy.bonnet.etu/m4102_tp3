package fr.ulille.iut.pizzaland;

import fr.ulille.iut.pizzaland.ApiV1;
import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzasCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzasDto;
import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizzas;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzasDao;

import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.TestProperties;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.Form;
import jakarta.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/*
 * JerseyTest facilite l'écriture des tests en donnant accès aux
 * méthodes de l'interface jakarta.ws.rs.client.Client.
 * la méthode configure() permet de démarrer la ressource à tester
 */
public class PizzaResourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(IngredientResourceTest.class.getName());
	private PizzasDao dao;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	// Les méthodes setEnvUp() et tearEnvDown() serviront à terme à initialiser la
	// base de données
	// et les DAO

	// https://stackoverflow.com/questions/25906976/jerseytest-and-junit-throws-nullpointerexception
	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzasDao.class);
		dao.createTableAndIngredientAssociation();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropPizzasTable();
	}

	@Test
	public void testGetEmptyList() {
		// La méthode target() permet de préparer une requête sur une URI.
		// La classe Response permet de traiter la réponse HTTP reçue.
		Response response = target("/pizzas").request().get();

		// On vérifie le code de la réponse (200 = OK)
		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		// On vérifie la valeur retournée (liste vide)
		// L'entité (readEntity() correspond au corps de la réponse HTTP.
		// La classe jakarta.ws.rs.core.GenericType<T> permet de définir le type
		// de la réponse lue quand on a un type paramétré (typiquement une liste).
		List<PizzasDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzasDto>>() {
		});

		assertEquals(0, pizzas.size());

	}

	@Test
	public void testGetExistingPizza() {

		Pizzas pizza = new Pizzas();
		pizza.setName("Licorne");
		dao.insertPizzas(pizza);

		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizzas result = Pizzas.fromDto(response.readEntity(PizzasDto.class));
		assertEquals(pizza.getId(),result.getId());
		assertEquals(pizza.getName(),result.getName());
	}

	@Test
	public void testGetNotExistingPizza() {
		Response response= target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(),response.getStatus());
	}

	@Test
	public void testCreatePizza() {
		PizzasCreateDto pizzaCreateDto = new PizzasCreateDto();
		pizzaCreateDto.setName("Licorne");

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzasDto returnedEntity = response.readEntity(PizzasDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(returnedEntity.getName(), pizzaCreateDto.getName());
	}

	@Test
	public void testCreateSamePizza() {
		Pizzas pizza = new Pizzas();
		pizza.setName("Licorne");
		dao.insertPizzas(pizza);

		PizzasCreateDto pizzaCreateDto = Pizzas.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreatePizzaWithoutName() {
		PizzasCreateDto pizzaCreateDto = new PizzasCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingPizza() {
		Pizzas pizza = new Pizzas();
		pizza.setName("Licorne");
		dao.insertPizzas(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizzas result = dao.findById(pizza.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetPizzaName() {
		Pizzas pizza = new Pizzas();
		pizza.setName("Licorne");
		dao.insertPizzas(pizza);

		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Licorne", response.readEntity(String.class));
	}


	@Test
	public void testGetNotExistingPizzaName() {
		Response response = target("pizzas").path(UUID.randomUUID().toString()).path("name").request().get();

		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateWithForm() {
		Form form = new Form();
		form.param("name", "Licorne");

		Entity<Form> formEntity = Entity.entity(form, MediaType.APPLICATION_FORM_URLENCODED_TYPE);
		Response response = target("pizzas").request().post(formEntity);

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		String location = response.getHeaderString("Location");
		String id = location.substring(location.lastIndexOf('/') + 1);
		Pizzas result = dao.findById(UUID.fromString(id));

		assertNotNull(result);
	}


}